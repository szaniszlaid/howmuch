package hu.szaniszlaid.howmuch.domain.account;

import hu.szaniszlaid.howmuch.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Account extends BaseEntity {

    @Column(unique=true)
    @NotNull
    private String name;
    private BigDecimal openingBalance;
    private String description;
}
