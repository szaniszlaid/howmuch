package hu.szaniszlaid.howmuch.domain.account;

import lombok.Getter;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;


/**
 * This query calculates the current balances of the accounts.
 * 1 - create table to summarize all incomes by account
 * 2 - create table to summarize all expenses by account
 * 3 - calculate the current balance as adding the opening balance to the incomes and after subtract the expenses by accounts
 * */
@Entity
@Getter
@Immutable
@Subselect(
        "with " +
                "incomes as(select sum(price) as sumincome, account_id from transaction tr where tr.type = 'INCOME'  group by account_id), " +
                "expenses as(select sum(price) as sumexpense, account_id from transaction tr where tr.type = 'EXPENSE'  group by account_id) " +

        "select " +
            "row_number() OVER () as id, " +
            "acc.id as account_id, " +
            "(coalesce(acc.opening_balance, 0) + coalesce(inc.sumincome, 0) - coalesce(ex.sumexpense, 0)) as balance " +
        "from account acc " +
        "left join incomes inc on inc.account_id = acc.id " +
        "left join expenses ex on ex.account_id = acc.id"
)
public class AccountBalanceDBView {

    @Id
    private Long id;

    private Long accountId;

    private BigDecimal balance;
}
