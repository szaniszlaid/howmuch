package hu.szaniszlaid.howmuch.domain.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface AccountBalanceDBViewRepository extends JpaRepository<AccountBalanceDBView, Long> {

    Optional<AccountBalanceDBView> findByAccountId(Long accountId);
}
