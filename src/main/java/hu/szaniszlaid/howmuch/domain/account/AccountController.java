package hu.szaniszlaid.howmuch.domain.account;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
public class AccountController {

    public static final String URL = "/account";

    private final AccountService accountService;
    private final AccountMapper accountMapper;

    public AccountController(AccountService accountService, AccountMapper accountMapper) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
    }

    @GetMapping(URL)
    public Collection<AccountResponse> getAll() {
        return convertAccountsToResponse(accountService.findAll());
    }

    // TODO mapstuct conversion
    private Collection<AccountResponse> convertAccountsToResponse(Collection<Account> accounts) {
       return accounts.stream()
                .map(this::convertAccountToResponse)
                .collect(Collectors.toList());
    }

    // TODO mapstuct conversion
    private AccountResponse convertAccountToResponse(Account account) {
        AccountResponse response = accountMapper.map(account);
        response.setBalance(accountService.getAccountBalance(account.getId()));
        return response;
    }

    @GetMapping(path = URL + "/{id}")
    public ResponseEntity<AccountResponse> getOne(@PathVariable("id") Long id) {
        return accountService.findById(id)
                .map(this::convertAccountToResponse)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(URL)
    public ResponseEntity<?> create(@Valid @RequestBody Account account) {
        boolean isNewEntity = account.getId() == null;
        if (!isNewEntity) {
            return ResponseEntity.badRequest().body("Id must be null on account creation");
        }

        Account saved = accountService.save(account);
        return new ResponseEntity<>(saved.getId(), HttpStatus.CREATED);
    }

    @PutMapping(URL)
    public ResponseEntity<?> update(@Valid @RequestBody Account account) {
        if (account.getId() == null) {
            return ResponseEntity.badRequest().build();
        }

        accountService.save(account);

        return ResponseEntity.accepted().build();
    }

    @DeleteMapping(path = URL + "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        accountService.deleteById(id);
        return ResponseEntity.accepted().build();
    }

    @GetMapping(URL + "/{id}" + "/balance")
    public ResponseEntity<?> getAccountBalance(@PathVariable("id") Long id) {
        return ResponseEntity.ok(accountService.getAccountBalance(id));
    }
}
