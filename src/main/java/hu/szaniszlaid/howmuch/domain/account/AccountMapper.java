package hu.szaniszlaid.howmuch.domain.account;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    AccountResponse map(Account account);

}
