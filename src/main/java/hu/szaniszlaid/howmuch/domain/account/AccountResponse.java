package hu.szaniszlaid.howmuch.domain.account;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountResponse {

    private Long id;
    private String name;
    private BigDecimal openingBalance;
    private BigDecimal balance;
    private String description;
}
