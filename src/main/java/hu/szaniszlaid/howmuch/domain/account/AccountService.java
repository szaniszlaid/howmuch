package hu.szaniszlaid.howmuch.domain.account;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;


@Service
public class AccountService {

    private final AccountBalanceDBViewRepository accountBalanceDBViewRepository;
    private final AccountRepository accountRepository;

    public AccountService(AccountBalanceDBViewRepository accountBalanceDBViewRepository,
                          AccountRepository accountRepository) {
        this.accountBalanceDBViewRepository = accountBalanceDBViewRepository;
        this.accountRepository = accountRepository;
    }

    public Account save(Account account) {
        BigDecimal openingBalance = account.getOpeningBalance() != null ? account.getOpeningBalance() : BigDecimal.ZERO;
        account.setOpeningBalance(openingBalance);
        return accountRepository.save(account);
    }

    public Collection<Account> findAll() {
        return accountRepository.findAll();
    }

    public Optional<Account> findById(long id) {
        return accountRepository.findById(id);
    }

    public void deleteById(long id) {
        accountRepository.deleteById(id);
    }

    public BigDecimal getAccountBalance(Long accountId) {
        return accountBalanceDBViewRepository
                .findByAccountId(accountId)
                .map(AccountBalanceDBView::getBalance)
                .orElse(BigDecimal.ZERO);
    }
}
