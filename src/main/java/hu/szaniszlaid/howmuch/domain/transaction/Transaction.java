package hu.szaniszlaid.howmuch.domain.transaction;

import hu.szaniszlaid.howmuch.domain.BaseEntity;
import hu.szaniszlaid.howmuch.domain.account.Account;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Transaction extends BaseEntity {

    @NotNull
    private LocalDateTime createdAt;

    @NotNull
    private BigDecimal price;

    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Account account;
}
