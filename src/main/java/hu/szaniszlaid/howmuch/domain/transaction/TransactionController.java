package hu.szaniszlaid.howmuch.domain.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collection;
import java.util.stream.Stream;

@CrossOrigin
@RestController
public class TransactionController {

    public static final String URL = "/transaction";

    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;

    @Autowired
    public TransactionController(TransactionService transactionService,
                                 TransactionMapper transactionMapper) {
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping(
            value = URL,
            params = {"page", "size"}
    )
    public Stream<Transaction> getTransactionPage(
            @RequestParam Integer page,
            @RequestParam Integer size,
            @RequestParam(required = false) String direction,
            @RequestParam(required = false) String... properties) {

        if (StringUtils.hasText(direction) && properties != null && properties.length > 0) {
            Sort.Direction sortDirection = Sort.Direction.fromString(direction);
            return transactionService.findByPage(page, size, sortDirection, properties).get();
        } else {
            return transactionService.findByPage(page, size).get();
        }
    }

    @GetMapping(URL)
    public Collection<TransactionDto> getTransactions(@RequestParam(required = false) Long accountId) {
        if (accountId != null) {
            return transactionMapper.map(transactionService.findAllByAccount(accountId));
        } else {
            return transactionMapper.map(transactionService.findAll());
        }
    }

    @PostMapping(URL)
    public ResponseEntity<?> saveTransaction(@Valid @RequestBody TransactionDto request) {
        Transaction saved = transactionService.save(transactionMapper.map(request));
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saved.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(URL)
    public ResponseEntity<?> deleteTransaction(@RequestParam Long id) {
        transactionService.delete(id);
        return ResponseEntity.accepted().build();
    }
}
