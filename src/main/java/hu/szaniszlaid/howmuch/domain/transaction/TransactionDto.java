package hu.szaniszlaid.howmuch.domain.transaction;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class TransactionDto {

    private Long id;

    @NotNull
    private LocalDateTime createdAt;

    @NotNull
    private Double price;

    private String description;

    @NotNull
    private TransactionType type;

    @NotNull
    private Long accountId;

}
