package hu.szaniszlaid.howmuch.domain.transaction;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
@DecoratedWith(TransactionMapperDecorator.class)
public interface TransactionMapper {

    @Mapping(target = "accountId", source = "account.id")
    TransactionDto map(Transaction transaction);
    Transaction map(TransactionDto transactionDto);

    Collection<TransactionDto> map(Collection<Transaction> transactions);


}
