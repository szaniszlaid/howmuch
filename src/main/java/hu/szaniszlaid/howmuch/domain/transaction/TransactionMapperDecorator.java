package hu.szaniszlaid.howmuch.domain.transaction;

import hu.szaniszlaid.howmuch.domain.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class TransactionMapperDecorator implements TransactionMapper {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    @Qualifier("delegate")
    private TransactionMapper delegate;

    /**
     * Sets the transaction's account ManyToOne by dto-s accountId property
     * */
    @Override
    public Transaction map(TransactionDto transactionDto) {
        Transaction transaction = delegate.map(transactionDto);
        transaction.setAccount(accountRepository.getOne(transactionDto.getAccountId()));

        return transaction;
    }
}
