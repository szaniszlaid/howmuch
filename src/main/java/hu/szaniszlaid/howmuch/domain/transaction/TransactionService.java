package hu.szaniszlaid.howmuch.domain.transaction;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class TransactionService {

    private final TransactionRepository repository;

    public TransactionService(TransactionRepository repository) {
        this.repository = repository;
    }

    public Collection<Transaction> findAll() {
        return repository.findAll();
    }

    public Collection<Transaction> findAllByAccount(Long accountId) {
        return repository.findAllByAccountId(accountId);
    }

    public Page<Transaction> findByPage(Integer page, Integer size, Sort.Direction direction,  String ... properties) {
        PageRequest request = PageRequest.of(page, size, direction, properties);
        return repository.findAll(request);
    }

    public Page<Transaction> findByPage(Integer page, Integer size) {
        return findByPage(page, size, Sort.Direction.DESC, "id");
    }

    public Transaction save(Transaction transaction) {
        return repository.save(transaction);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
}
