package hu.szaniszlaid.howmuch.domain.transaction;

public enum TransactionType {
    EXPENSE, INCOME, TRANSFER
}
