package hu.szaniszlaid.howmuch.domain.transactioncategory;

import hu.szaniszlaid.howmuch.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@ToString
public class TransactionCategory extends BaseEntity {

    @NotNull
    private String name;

    @ManyToOne
    private TransactionCategory parent;
}
