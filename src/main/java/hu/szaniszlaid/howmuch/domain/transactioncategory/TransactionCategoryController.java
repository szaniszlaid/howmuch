package hu.szaniszlaid.howmuch.domain.transactioncategory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

@CrossOrigin
@RestController
public class TransactionCategoryController {
    private final TransactionCategoryService transactionCategoryService;

    @Autowired
    public TransactionCategoryController(TransactionCategoryService transactionCategoryService) {
        this.transactionCategoryService = transactionCategoryService;
    }

    @GetMapping(
            value = "/transactioncategory",
            params = {"page", "size"}
    )
    public Stream<TransactionCategory> getTransactionCategoryPage(
            @RequestParam Integer page,
            @RequestParam Integer size,
            @RequestParam(required = false) Optional<String> direction,
            @RequestParam(required = false) String ... properties) {

        if (direction.isPresent() && properties.length > 0) {
            Sort.Direction sortDirection = Sort.Direction.fromString(direction.get());
            return transactionCategoryService.findByPage(page, size, sortDirection, properties).get();
        } else {
            return transactionCategoryService.findByPage(page, size).get();
        }
    }

    @GetMapping("/transactioncategory")
    public Collection<TransactionCategory> getTransactionPage() {
        return transactionCategoryService.findAll();
    }

    @PutMapping(value = "/transactioncategory")
    public void createTransactionCategory(TransactionCategory transactionCategory) {
        transactionCategoryService.saveTransactionCategory(transactionCategory);
    }
}
