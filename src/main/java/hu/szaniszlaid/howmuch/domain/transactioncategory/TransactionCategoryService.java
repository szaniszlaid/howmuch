package hu.szaniszlaid.howmuch.domain.transactioncategory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class TransactionCategoryService {

    private final TransactionCategoryRepository repository;

    public TransactionCategoryService(TransactionCategoryRepository repository) {
        this.repository = repository;
    }

    public Collection<TransactionCategory> findAll() {
        return repository.findAll();
    }

    public Page<TransactionCategory> findByPage(Integer page, Integer size, Sort.Direction direction,  String ... properties) {
        PageRequest request = PageRequest.of(page, size, direction, properties);
        return repository.findAll(request);
    }

    public Page<TransactionCategory> findByPage(Integer page, Integer size) {
        return findByPage(page, size, Sort.Direction.DESC, "id");
    }

    public void saveTransactionCategory(TransactionCategory transactionCategory) {
        repository.save(transactionCategory);
    }
}
