package hu.szaniszlaid.howmuch.validation;

import lombok.*;

import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Builder
@ToString
public class ErrorDetails {
    @Builder.Default
    private Date timestamp = new Date();
    private String message;
    @Singular
    private Collection<FieldErrorDetails> fieldErrorDetails;
}