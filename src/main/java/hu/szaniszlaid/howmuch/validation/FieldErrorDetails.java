package hu.szaniszlaid.howmuch.validation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.springframework.validation.FieldError;

@Getter
@Builder
@ToString
@AllArgsConstructor
class FieldErrorDetails {

    public FieldErrorDetails(FieldError fieldError) {
        this.fieldName = fieldError.getField();
        this.rejectedValue = fieldError.getRejectedValue();
        this.message = fieldError.getDefaultMessage();
    }

    private final String fieldName;
    private final Object rejectedValue;
    private final String message;
}
