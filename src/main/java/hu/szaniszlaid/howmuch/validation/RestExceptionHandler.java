package hu.szaniszlaid.howmuch.validation;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@ControllerAdvice
@RestController
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        ErrorDetails errorDetails = ErrorDetails.builder()
                .message("Validation Failed")
                .fieldErrorDetails(
                        ex.getBindingResult().getFieldErrors()
                                .stream()
                                .map(FieldErrorDetails::new)
                                .collect(Collectors.toList()))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity<Object> handleEmptyResultDataAccessException(HttpServletRequest request, EmptyResultDataAccessException ex) {
        ErrorDetails errorDetails = ErrorDetails.builder()
                .message("Entity does not exists!")
                .fieldErrorDetail(FieldErrorDetails.builder()
                        .fieldName("id")
                        .message(ex.getMessage())
                        .rejectedValue(request.getParameter("id"))
                        .build())
                .build();

        return new ResponseEntity<>(errorDetails, HttpStatus.NO_CONTENT);
    }
}
