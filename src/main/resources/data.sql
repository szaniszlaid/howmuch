insert into account(name, description, opening_balance) values ('Test account', 'This is the first of all', 1000);
insert into account(name, description, opening_balance) values ('Second account', 'For testing', 562);

insert into transaction(account_id, description, created_at, price, type) values (1, 'Mozijegy', '2018-05-11 16:43', 1653, 'INCOME');
insert into transaction(account_id, description, created_at, price, type) values (1, 'Bevásárlás', '2018-06-18 22:43', 1500, 'INCOME');
insert into transaction(account_id, description, created_at, price, type) values (1, 'Sör', '2018-07-11 18:38', 3000, 'EXPENSE');
insert into transaction(account_id, description, created_at, price, type) values (1, 'Fizetés', '2018-07-11 18:50', 500000, 'INCOME');

insert into transaction(account_id, description, created_at, price, type) values (2, 'Water bill', '2098-07-11 18:50', 12000, 'EXPENSE');


insert into transaction_category (name) values ('Bevásárlás');
insert into transaction_category (name) values ('Étterem');
insert into transaction_category (name) values ('Ajándék');
