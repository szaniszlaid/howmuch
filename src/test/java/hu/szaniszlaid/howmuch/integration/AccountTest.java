package hu.szaniszlaid.howmuch.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.szaniszlaid.howmuch.HowMuchApplication;
import hu.szaniszlaid.howmuch.domain.account.Account;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionController;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionDto;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionRepository;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static hu.szaniszlaid.howmuch.domain.account.AccountController.URL;
import static hu.szaniszlaid.howmuch.unit.utils.MatcherUtils.isNumberEqual;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = HowMuchApplication.class)
public class AccountTest {

    @Autowired
    private ObjectMapper jacksonObjectMapper;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final MultiValueMap<String, String> HEADER = new LinkedMultiValueMap<>();
    private static final Account INITIAL_ACCOUNT_1;
    private static final Account INITIAL_ACCOUNT_2;

    private static boolean initialized;

    static {
        // set content type to json
        HEADER.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        // Initial accounts will be available in the whole test class
        INITIAL_ACCOUNT_1 = new Account();
        INITIAL_ACCOUNT_1.setDescription("First test acc");
        INITIAL_ACCOUNT_1.setName("Acc 1");
        INITIAL_ACCOUNT_1.setOpeningBalance(BigDecimal.valueOf(1500));

        INITIAL_ACCOUNT_2 = new Account();
        INITIAL_ACCOUNT_2.setDescription("Second test acc");
        INITIAL_ACCOUNT_2.setName("Acc 2");
        INITIAL_ACCOUNT_2.setOpeningBalance(BigDecimal.valueOf(100.5));
    }

    @BeforeEach
    public void init() throws JsonProcessingException {
        // cleanup transactions
        transactionRepository.deleteAll();

        // Post initial accounts
        if (!initialized) {
            postAccount(INITIAL_ACCOUNT_1);
            postAccount(INITIAL_ACCOUNT_2);
            initialized = true;
        }
    }

    @Test
    public void postAccount_shouldReturnId() throws Exception {
        Account account = new Account();
        account.setDescription("Third test acc");
        account.setName("Acc 3");

        mockMvc.perform(MockMvcRequestBuilders.post(URL)
                .content(jacksonObjectMapper.writeValueAsString(account))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", isNumberEqual(3)));
    }

    @Test
    public void getAccountBalance_shouldReturnInitialAccountsOpeningBalance() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(URL + "/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", isNumberEqual(INITIAL_ACCOUNT_1.getOpeningBalance())));
    }

    @Test
    public void getAccountBalance_shouldBeCalculatedByIncomesAndExpenses() throws Exception {
        // sum expenses 30.99
        double expPrice1 = 25.99;
        double expPrice2 = 5;

        // sum incomes 10.15
        double income1 = 0.15;
        double income2 = 10;

        // current balance = opening balance - expenses + incomes
        double sumBalance = 1479.16;

        // post expenses
        postTransaction(expPrice1, TransactionType.EXPENSE);
        postTransaction(expPrice2, TransactionType.EXPENSE);

        // post incomes
        postTransaction(income1, TransactionType.INCOME);
        postTransaction(income2, TransactionType.INCOME);

        mockMvc.perform(MockMvcRequestBuilders.get(URL + "/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", isNumberEqual(sumBalance)));
    }

    @Test
    public void testAccountWithNegativeBalance() throws Exception {
        // sum expenses 10,005.1
        double expPrice1 = 10_000;
        double expPrice2 = 5.1;

        // sum incomes 0.15
        double income1 = 0.15;

        // current balance = opening balance - expenses + incomes
        double sumBalance = -8_504.95;

        // post expenses
        postTransaction(expPrice1, TransactionType.EXPENSE);
        postTransaction(expPrice2, TransactionType.EXPENSE);

        // post incomes
        postTransaction(income1, TransactionType.INCOME);

        mockMvc.perform(MockMvcRequestBuilders.get(URL + "/1/balance"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", isNumberEqual(sumBalance)));

    }


    private void postTransaction(Double price, TransactionType type) throws Exception {
        TransactionDto transaction = new TransactionDto();
        transaction.setPrice(price);
        transaction.setType(type);
        transaction.setAccountId(INITIAL_ACCOUNT_1.getId());
        transaction.setCreatedAt(LocalDateTime.now());

        HttpEntity<String> transactionToPost = new HttpEntity<>(jacksonObjectMapper.writeValueAsString(transaction), HEADER);
        restTemplate.postForEntity(TransactionController.URL, transactionToPost, Object.class);
    }

    private void postAccount(Account account) throws JsonProcessingException {
        HttpEntity<String> accountToPost = new HttpEntity<>(jacksonObjectMapper.writeValueAsString(account), HEADER);
        Number id = restTemplate.postForObject(URL, accountToPost, Number.class);
        account.setId(id.longValue());
    }
}
