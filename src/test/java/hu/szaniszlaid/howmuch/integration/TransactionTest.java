package hu.szaniszlaid.howmuch.integration;

import hu.szaniszlaid.howmuch.domain.transaction.Transaction;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionRepository;
import hu.szaniszlaid.howmuch.unit.transaction.Utils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransactionTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TransactionRepository repository;

    @Test
    public void test() {
        Transaction t1 = Utils.createAppropriateTransaction(1.0);
        Transaction t2 = Utils.createAppropriateTransaction(3.0);

        repository.save(t1);
        repository.save(t2);

        ResponseEntity<Transaction[]> response = restTemplate.getForEntity("/transaction", Transaction[].class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody().length).isEqualTo(2);
        Set<Transaction> body = new HashSet<>(Arrays.asList(response.getBody()));
        Assertions.assertThat(body).containsExactlyInAnyOrder(t1, t2);
    }
}
