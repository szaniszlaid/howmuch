package hu.szaniszlaid.howmuch.unit.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.szaniszlaid.howmuch.HowMuchApplication;
import hu.szaniszlaid.howmuch.domain.account.Account;
import hu.szaniszlaid.howmuch.domain.account.AccountController;
import hu.szaniszlaid.howmuch.domain.account.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static hu.szaniszlaid.howmuch.domain.account.AccountController.URL;
import static hu.szaniszlaid.howmuch.unit.utils.MatcherUtils.isNumberEqual;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = HowMuchApplication.class)
public class ControllerTest {

    @MockBean
    private AccountService accountService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jacksonObjectMapper;

    //GET
    @Test
    public void getAllTest() throws Exception {
        Account account = new Account();
        account.setId(1L);
        account.setName("a1");
        account.setOpeningBalance(BigDecimal.valueOf(3.14));
        account.setDescription("a1 test description");

        given(accountService.findAll()).willReturn(Collections.singletonList(account));

        mockMvc.perform(MockMvcRequestBuilders.get(URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].id", isNumberEqual(account.getId())))
                .andExpect(jsonPath("$.[0].name", is(account.getName())))
                .andExpect(jsonPath("$.[0].openingBalance", is(account.getOpeningBalance().doubleValue())))
                .andExpect(jsonPath("$.[0].description", is(account.getDescription())));
    }

    @Test
    public void getOneWithExisting_shouldReturnTheCompleteAccount() throws Exception {
        Account account = new Account();
        account.setName("Sample");
        account.setOpeningBalance(BigDecimal.valueOf(5000.8));
        account.setDescription("Get one test description");
        account.setId(1L);

        given(accountService.findById(1L)).willReturn(Optional.of(account));

        mockMvc.perform(MockMvcRequestBuilders
                .get(URL + "/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", isNumberEqual(account.getId())))
                .andExpect(jsonPath("name", is(account.getName())))
                .andExpect(jsonPath("openingBalance", isNumberEqual(account.getOpeningBalance())))
                .andExpect(jsonPath("description", is(account.getDescription())));

    }

    //POST
    @Test
    public void postNewAccount_shouldReturnWithIdAndHttpStatusCreated() throws Exception {
        Account newAccount = new Account();
        newAccount.setName("sample");

        Account savedAccount = new Account();
        savedAccount.setId(2L);

        given(accountService.save(newAccount)).willReturn(savedAccount);

        mockMvc.perform(MockMvcRequestBuilders.post(URL)
                .content(jacksonObjectMapper.writeValueAsString(newAccount))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().string(is("2")));
    }

    @Test
    public void postWithId_shouldReturnBadRequest() throws Exception {
        Account newAccount = new Account();
        newAccount.setName("sample");
        newAccount.setId(1L);

        verify(accountService, never()).save(any());

        mockMvc.perform(MockMvcRequestBuilders.post(URL)
                .content(jacksonObjectMapper.writeValueAsString(newAccount))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(is("Id must be null on account creation")));
    }

    @Test
    public void postAccountWithoutName_shouldReturnBadRequest() throws Exception {
        Account account = new Account();

        mockMvc.perform(MockMvcRequestBuilders.post(URL)
                .content(jacksonObjectMapper.writeValueAsString(account))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(accountService, never()).save(any());
    }

    //PUT
    @Test
    public void putExistingAccount_shouldReturnAccepted() throws Exception {
        Account account = new Account();
        account.setId(1L);
        account.setName("Sample");

        given(accountService.save(account)).willReturn(account);

        mockMvc.perform(MockMvcRequestBuilders.put(URL)
                .content(jacksonObjectMapper.writeValueAsString(account))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted());
    }

    @Test
    public void putWithoutId_shouldReturnBadRequest() throws Exception {
        Account account = new Account();
        account.setName("Sample");

        mockMvc.perform(MockMvcRequestBuilders.put(URL)
                .content(jacksonObjectMapper.writeValueAsString(account))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void putWithoutName_shouldReturnBadRequest() throws Exception {
        Account account = new Account();
        account.setName("Sample");

        mockMvc.perform(MockMvcRequestBuilders.put(URL)
                .content(jacksonObjectMapper.writeValueAsString(account))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteTransactionByExistingId_shouldDeleteAndReturnHttpAccepted() throws Exception {

        doNothing().when(accountService).deleteById(1);

        mockMvc.perform(MockMvcRequestBuilders.delete(URL + "/1"))
                .andExpect(status().isAccepted());

        verify(accountService, times(1)).deleteById(1);

    }

}
