package hu.szaniszlaid.howmuch.unit.account;

import hu.szaniszlaid.howmuch.domain.account.Account;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class Pojo {

    private static final Long ID = 1L;
    private static final String NAME = "Sample account";

    @Test
    public void toString_shouldContainIdAndName() {
        Account account = new Account();
        account.setId(ID);
        account.setName(NAME);

        Assertions.assertThat(account.toString()).contains(Long.toString(ID));
        Assertions.assertThat(account.toString()).contains(NAME);
    }
}
