package hu.szaniszlaid.howmuch.unit.account;

import hu.szaniszlaid.howmuch.domain.account.Account;
import hu.szaniszlaid.howmuch.domain.account.AccountRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class RepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void saveAndFindAll_shouldReturnAllValuesWithFilledProperties() {
        Account account = new Account();
        account.setName("a1");
        account.setOpeningBalance(BigDecimal.valueOf(3.14));
        account.setDescription("a1 test description");

        entityManager.persistAndFlush(account);
        Account retrieved = accountRepository.findAll().get(0);

        assertThat(retrieved).isNotNull();
        assertThat(retrieved.getName()).isEqualTo(account.getName());
        assertThat(retrieved.getOpeningBalance()).isEqualTo(account.getOpeningBalance());
        assertThat(retrieved.getDescription()).isEqualTo(account.getDescription());
    }

    @Test
    public void saveWithoutName_shouldFailBecauseNameMustNotBeNull() {
        Account account = new Account();
        account.setName(null);

        Assertions.assertThrows(ConstraintViolationException.class, () -> entityManager.persistAndFlush(account));
    }

    @Test
    public void saveAccountsWithSimilarName_shouldFailCauseAccountNameMustBeUnique() {
        String nonUniqueName = "Sample account";

        Account a1 = new Account();
        a1.setName(nonUniqueName);

        Account a2 = new Account();
        a2.setName(nonUniqueName);

        Assertions.assertThrows(PersistenceException.class, () -> {
            entityManager.persistAndFlush(a1);
            entityManager.persistAndFlush(a2);
        });
    }
}
