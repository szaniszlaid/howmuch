package hu.szaniszlaid.howmuch.unit.account;

import hu.szaniszlaid.howmuch.domain.account.Account;
import hu.szaniszlaid.howmuch.domain.account.AccountRepository;
import hu.szaniszlaid.howmuch.domain.account.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ServiceTest {

    @InjectMocks
    private AccountService service;

    @Mock
    private AccountRepository repository;


    @Test
    public void findAll_shouldCallRepository() {
        service.findAll();
        verify(repository, times(1)).findAll();
    }

    @Test
    public void saveShouldCallRepository() {
        Account account = new Account();
        service.save(account);
        verify(repository, times(1)).save(account);
    }

}
