package hu.szaniszlaid.howmuch.unit.transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.szaniszlaid.howmuch.HowMuchApplication;
import hu.szaniszlaid.howmuch.domain.transaction.*;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = HowMuchApplication.class)
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jacksonObjectMapper;

    @MockBean
    private TransactionService transactionService;

    @Test
    public void getAllTransaction_shouldReturnAll() throws Exception {
        Transaction t1 = Utils.createAppropriateTransaction(3.4);
        Transaction t2 = Utils.createAppropriateTransaction(7.6);

        given(transactionService.findAll()).willReturn(Arrays.asList(t1, t2));

        mockMvc.perform(MockMvcRequestBuilders.get("/transaction"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getTransactionPagedWithAllParams_shouldReturnPage() throws Exception {
        Transaction t1 = Utils.createAppropriateTransaction(1.0);
        Transaction t2 = Utils.createAppropriateTransaction(2.0);

        Integer page = 1;
        Integer size = 2;
        Sort.Direction direction = Sort.Direction.DESC;
        String[] properties = {"id"};

        given(transactionService.findByPage(page, size, direction, properties))
                .willReturn(new PageImpl<>(Arrays.asList(t1, t2)));

        mockMvc.perform(MockMvcRequestBuilders.get("/transaction")
                .param("page", page.toString())
                .param("size", size.toString())
                .param("direction", direction.toString())
                .param("properties", properties))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getTransactionWithPageAndSize_shouldReturnPage() throws Exception {
        Transaction t1 = Utils.createAppropriateTransaction(1.0);
        Transaction t2 = Utils.createAppropriateTransaction(2.0);

        Integer page = 0;
        Integer size = 2;

        given(transactionService.findByPage(page, size)).willReturn(new PageImpl<>(Arrays.asList(t1, t2)));

        mockMvc.perform(MockMvcRequestBuilders.get("/transaction")
                .param("page", page.toString())
                .param("size", size.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void putNewTransaction_shouldReturnCreated() throws Exception {
        TransactionDto t1 = new TransactionDto();
        t1.setType(TransactionType.INCOME);
        t1.setPrice(123.45);
        t1.setCreatedAt(LocalDateTime.now());
        t1.setAccountId(1L);

        Transaction savedTransaction = new Transaction();
        savedTransaction.setId(1L);
        given(transactionService.save(any())).willReturn(savedTransaction);

        mockMvc.perform(MockMvcRequestBuilders.post("/transaction")
                .content(jacksonObjectMapper.writeValueAsString(t1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", Matchers.endsWith("/transaction/" + savedTransaction.getId())));
    }

    @Test
    public void putTransactionWithoutDateAndPrice_shouldFail() throws Exception {
        TransactionDto t1 = new TransactionDto();
        t1.setAccountId(1L);
        t1.setCreatedAt(null);
        t1.setPrice(null);
        t1.setType(TransactionType.EXPENSE);

        mockMvc.perform(MockMvcRequestBuilders.post("/transaction")
                .content(jacksonObjectMapper.writeValueAsString(t1))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrorDetails", hasSize(2)));
    }

    @Test
    public void deleteTransactionByExistingId_shouldDeleteAndReturnHttpAccepted() throws Exception {
        doNothing().when(transactionService).delete(any());

        mockMvc.perform(MockMvcRequestBuilders.delete("/transaction")
                .param("id", "1"))
                .andExpect(status().isAccepted());
    }

    @Test
    public void deleteTransactionByNonExistingId_shouldFail() throws Exception {
        doThrow(new EmptyResultDataAccessException(1)).when(transactionService).delete(any());
        mockMvc.perform(MockMvcRequestBuilders.delete("/transaction")
                .param("id", "1"))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$.fieldErrorDetails[0].rejectedValue", is("1")));
    }
}
