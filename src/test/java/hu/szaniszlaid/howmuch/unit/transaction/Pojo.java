package hu.szaniszlaid.howmuch.unit.transaction;

import hu.szaniszlaid.howmuch.domain.transaction.Transaction;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Pojo {


    @Test
    public void toString_shouldContainIdAndPrice() {
        Transaction transaction = new Transaction();
        transaction.setId(1L);
        transaction.setPrice(BigDecimal.valueOf(23.7));
        transaction.setDescription("test description");
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setType(TransactionType.EXPENSE);

        Assertions.assertThat(transaction.toString()).contains(transaction.getId().toString());
        Assertions.assertThat(transaction.toString()).contains(transaction.getPrice().toString());
    }
}
