package hu.szaniszlaid.howmuch.unit.transaction;

import hu.szaniszlaid.howmuch.domain.transaction.Transaction;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionRepository;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class RepositoryTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findAllByPageTest() {
        insertTransactions();

        Page<Transaction> page = transactionRepository.findAll(PageRequest.of(0, 2));

        assertThat(page.getTotalElements()).isEqualTo(20);
        assertThat(page.getNumberOfElements()).isEqualTo(2);
    }

    private void insertTransactions() {
        for (int i = 0; i < 20; i++) {
            Transaction transaction = new Transaction();
            transaction.setCreatedAt(LocalDateTime.now());
            transaction.setPrice(BigDecimal.valueOf(i));
            transaction.setType(TransactionType.EXPENSE);

            entityManager.persist(transaction);
        }

        entityManager.flush();
    }

    @Test
    public void findById_shouldReturnValueWithAppropriateProperties() {
        Transaction transaction = Utils.createAppropriateTransaction();

        Long savedId = entityManager.persistAndFlush(transaction).getId();

        Optional<Transaction> retrievedOptional = transactionRepository.findById(savedId);
        Assertions.assertTrue(retrievedOptional.isPresent());

        Transaction retrieved = retrievedOptional.get();

        assertThat(retrieved.getPrice()).isEqualTo(transaction.getPrice());
        assertThat(retrieved.getDescription()).isEqualTo(transaction.getDescription());
        assertThat(retrieved.getCreatedAt()).isEqualTo(transaction.getCreatedAt());
        assertThat(retrieved.getType()).isEqualTo(transaction.getType());
    }

    @Test
    public void saveNullPrice_shouldThrowNotNullException() {
        Transaction transaction = Utils.createAppropriateTransaction();
        transaction.setPrice((BigDecimal) null);
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            transactionRepository.save(transaction);
            entityManager.flush();
        });

    }

    @Test
    public void saveNullDate_shouldThrowNotNullException() {
        Transaction transaction = Utils.createAppropriateTransaction();
        transaction.setCreatedAt(null);
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            transactionRepository.save(transaction);
            entityManager.flush();
        });
    }

//    @Test
//    public void findAllByExampleTest() {
//        fail("Not yet implemented");
//    }
}
