package hu.szaniszlaid.howmuch.unit.transaction;

import hu.szaniszlaid.howmuch.domain.transaction.Transaction;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionRepository;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ServiceTest {

    @Mock
    private TransactionRepository repository;

    private TransactionService service;
    private Transaction transaction;

    @BeforeEach
    public void setUp() {
        this.service = new TransactionService(repository);
        transaction = Utils.createAppropriateTransaction(1.0);
    }

    @Test
    public void findAll_shouldReturnAllTransaction() {
        Transaction transaction2 = Utils.createAppropriateTransaction(2.0);

        given(repository.findAll()).willReturn(Arrays.asList(transaction, transaction2));

        assertThat(service.findAll()).containsExactlyInAnyOrder(transaction, transaction2);
    }

    @Test
    public void findByPageWithoutProperties_shouldReturnOrderedByIdDescending() {
        Transaction transaction2 = Utils.createAppropriateTransaction(2.0);

        int pageCount = 0;
        int size = 5;

        PageRequest request = PageRequest.of(pageCount, size, Sort.Direction.DESC, "id");
        given(repository.findAll(request)).willReturn(new PageImpl<>(Arrays.asList(transaction, transaction2), request, 10));

        Page<Transaction> page = service.findByPage(pageCount, size);

        Optional<Sort.Order> orderOpt = page.getPageable().getSort().get().findFirst();

        assertThat(orderOpt).isPresent();

        orderOpt.ifPresent(order -> {
            assertThat(order.getProperty()).isEqualTo("id");
            assertThat(order.getDirection()).isEqualTo(Sort.Direction.DESC);
        } );

    }

    @Test
    public void saveTransaction_shouldReturnEntity() {
        given(repository.save(transaction)).willReturn(transaction);
        Transaction saved = service.save(transaction);
        Assertions.assertThat(saved).isEqualTo(saved);
    }

    @Test
    public void delete_shouldCallRepository() {
        service.delete(1L);
        verify(repository, times(1)).deleteById(1L);
    }
}
