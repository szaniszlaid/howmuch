package hu.szaniszlaid.howmuch.unit.transaction;

import hu.szaniszlaid.howmuch.domain.transaction.Transaction;
import hu.szaniszlaid.howmuch.domain.transaction.TransactionType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Utils {

    public static Transaction createAppropriateTransaction() {
        return createAppropriateTransaction(3.14);
    }

    public static Transaction createAppropriateTransaction(Double appendix) {
        Transaction transaction = new Transaction();
        transaction.setPrice(BigDecimal.valueOf(appendix));
        transaction.setDescription("test description" + appendix);
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setType(TransactionType.EXPENSE);

        return transaction;
    }
}
