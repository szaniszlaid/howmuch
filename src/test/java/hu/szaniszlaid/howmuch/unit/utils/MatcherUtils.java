package hu.szaniszlaid.howmuch.unit.utils;


import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.math.BigDecimal;

public class MatcherUtils {

    public static Matcher<Number> isNumberEqual(final Number value) {
        return new TypeSafeMatcher<>() {

            @Override
            public void describeTo(Description description) {
                description
                        .appendText("isNumberEqual should return ")
                        .appendValue(value);
            }

            @Override
            protected boolean matchesSafely(Number item) {
                if (value instanceof BigDecimal) {
                    return ((BigDecimal) value).compareTo(BigDecimal.valueOf((double) item)) == 0;
                } else if (value instanceof Double) {
                    return ((Double) value).compareTo((double) item) == 0;
                } else {
                    return item.longValue() == value.longValue();
                }
            }

        };
    }
}

